FROM docker:20

RUN apk add --update alpine-sdk py3-pip py3-netaddr python3-dev jq yamllint \
    && python -m pip install --upgrade pip \
    && pip install --upgrade wheel \
    && pip install ansible==4.7.0 ansible-core==2.11.8 ansible-lint==5.3.2 ansible-modules-hashivault==4.6.6
