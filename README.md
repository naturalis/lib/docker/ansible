# Docker Ansible

Creates a docker image for deploying Ansible and provide linting support.

NOTE: as base image the [docker image](https://hub.docker.com/_/docker) is used, which allows this image to be run in a docker container.

## Package versions

Ansible: 4.7.0
Ansible-core: 2.11.8
Ansible-hashivault-module: 4.6.6
Ansible-lint: 5.3.2
Docker: 20

## Tags

We use the following tags
- .latest: the most recent build
- .stable: the tested and tagged stable version (recommended)
- .<version>: Use a specific version of past stable releases

## Build

Any pushes to main branch will create a new .latest
Any tags will create a .stable and a .<tag> version

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

 * `pre-commit autoupdate`
 * `pre-commit install`
